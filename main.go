package main

import (
	"example.com/mod/routes"
	"net/http"
)

func main() {

	routes.InitRouter()
	http.ListenAndServe(":8080", nil)

}
