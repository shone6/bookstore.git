# bookstore

#### 介绍
Golang开发的书店页面

#### 软件架构
软件架构说明

controller 控制器 

dao 数据库访问层

model 模型文件 

views 前端代码

utils 连接数据库的代码和生成UUID的代码

routes 路由函数

#### 安装教程

git clone https://gitee.com/shone6/bookstore.git

#### 使用说明

创建好对应的数据库表之后就可以运行了
首先 go build main.go


然后 main.exe




