package model

//CartItem 购物项结构体
type CartItem struct {
	CartItemID int64   //购物项的id
	Book       *Book   //购物项中的图书信息
	Count      int64   //购物项中图书的数量
	Amount     float64 //购物项中图书的金额小计，通过计算得到
	CartID     string  //当前购物项属于哪一个购物车
}

//GetAmount 获取购物项中图书的金额小计，有图书的价格和图书的数量计算得到
func (cartItem *CartItem) GetAmount() float64 {
	//获取当前购物项中图书的价格
	price := cartItem.Book.Price
	return float64(cartItem.Count) * price
}

//Cart 购物车结构体
type Cart struct {
	CartID      string      //购物车的id
	CartItems   []*CartItem //购物车中所有的购物项
	TotalCount  int64       //购物车中图书的总数量，通过计算得到
	TotalAmount float64     //购物车中图书的总金额，通过计算得到
	UserID      int         //当前购物车所属的用户
}

//GetTotalCount 获取购物车中图书的总数量
func (cart *Cart) GetTotalCount() int64 {
	var totalCount int64
	//遍历购物车中的购物项切片
	for _, v := range cart.CartItems {
		totalCount = totalCount + v.Count
	}
	return totalCount
}

//GetTotalAmount 获取购物车中图书的总金额
func (cart *Cart) GetTotalAmount() float64 {
	var totalAmount float64
	//遍历购物车中的购物项切片
	for _, v := range cart.CartItems {
		totalAmount = totalAmount + v.GetAmount()
	}
	return totalAmount

}
